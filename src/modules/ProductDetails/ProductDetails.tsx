import Link from "next/link";
import { FC, useEffect } from "react";
import { Button, IconCart, IconLeftArrow, PageLoader } from "~/components";
import { API_URL_PRODUCT } from "~/constants";
import { useCartContext } from "~/context/CartContext";
import { useFetch } from "~/hooks/useFetch";
import { getFormattedPrice } from "~/util/getFormattedPrice";

const getProductDetails = (gtin: string) => {
  return fetch(`${API_URL_PRODUCT}/${gtin}`);
};

interface IProps {
  gtin: string;
}

const ProductDetails: FC<IProps> = ({ gtin }) => {
  const { cart, updateCart } = useCartContext();

  const { loading, data: product, request } = useFetch(getProductDetails);

  useEffect(() => {
    request(gtin);
  }, [gtin]);

  const handleAddToCart = () => {
    const quantity = cart?.[gtin] ? cart[gtin].quantity + 1 : 1;
    updateCart({
      ...product,
      quantity,
    });
  };

  if (loading) {
    return <PageLoader />;
  }

  if (!product) {
    return null;
  }

  const { name, imageUrl, recommendedRetailPrice, recommendedRetailPriceCurrency, categoryName } =
    product || {};

  return (
    <section className="text-gray-700 body-font overflow-hidden bg-white">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex justify-between mt-10 mb-5 ml-10 md:ml-0">
          <Link href="/">
            <a href="#" className="flex font-semibold text-indigo-600 text-sm mt-10">
              <IconLeftArrow />
              Back
            </a>
          </Link>
        </div>
        <div className="lg:w-4/5 mx-auto flex flex-wrap">
          <img
            alt="ecommerce"
            className="lg:w-1/2 w-full object-cover object-center"
            src={imageUrl}
          ></img>
          <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
            <h2 className="text-sm title-font text-gray-500 tracking-widest"></h2>
            <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">{name}</h1>

            <p className="leading-relaxed">{categoryName}</p>

            <div className="flex justify-between mt-20">
              <span className="title-font font-medium text-2xl text-gray-900">
                {getFormattedPrice(recommendedRetailPrice, recommendedRetailPriceCurrency)}
              </span>
              <Button
                styles=" inline-flex items-center "
                disabled={loading}
                onClick={handleAddToCart}
              >
                <IconCart />
                <span className="ml-1">{loading ? "Adding" : "Add to cart"}</span>
              </Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductDetails;

import { FC, useEffect } from "react";
import { PageLoader, Pagination, ProductCard } from "~/components";
import { IProduct } from "~/components/types";
import { API_URL_PRODUCT } from "~/constants";
import { useCartContext } from "~/context/CartContext";
import { useFetch } from "~/hooks/useFetch";

const getProducts = (page: number) => {
  return fetch(`${API_URL_PRODUCT}?page=${page || 1}`);
};

interface IProps {
  page: number;
}

const Products: FC<IProps> = ({ page }) => {
  const { cart, updateCart } = useCartContext();
  const { loading, data, request } = useFetch(getProducts);

  useEffect(() => {
    request(page);
  }, [page]);

  const handleAddToCart = (product: IProduct) => {
    const quantity = cart?.[product.gtin] ? cart[product.gtin].quantity + 1 : 1;

    updateCart({
      ...product,
      quantity,
    });
  };

  if (loading) {
    return <PageLoader />;
  }

  const hasProducts = data?.results?.length > 0;

  return (
    <section className=" py-8">
      <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12 mb-8">
        <nav id="store" className="w-full z-30 top-0 px-6 py-1">
          <div className="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-2 py-3">
            <a
              className="uppercase tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl "
              href="#"
            >
              Store
            </a>
          </div>
        </nav>

        {!hasProducts ? (
          <div className="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-2 py-3">
            <h1 className="text-center text-gray-800 text-xl">No Products Found</h1>
          </div>
        ) : (
          <>
            <div className="px-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6">
              {data?.results?.map((product: any) => {
                return (
                  <ProductCard
                    key={product.id}
                    product={product}
                    handleAddToCart={handleAddToCart}
                  />
                );
              })}
            </div>
          </>
        )}
      </div>
      {hasProducts && <Pagination currentPage={page} totalCount={data?.count} />}
    </section>
  );
};

export default Products;

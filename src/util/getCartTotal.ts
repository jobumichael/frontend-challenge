import { IProduct } from "~/components/types";
import { getFormattedPrice } from "./getFormattedPrice";

interface IGetCartQty extends IProduct {
  quantity: number;
}

export const getCartTotal = (products: IGetCartQty[]) => {
  const totalPrice = products.reduce(
    (acc, item) => acc + item.quantity * item.recommendedRetailPrice,
    0
  );

  return getFormattedPrice(totalPrice);
};

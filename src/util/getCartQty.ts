import { IProduct } from "~/components/types";

interface IGetCartQty extends IProduct {
  quantity: number;
}

export const getCartQty = (products: IGetCartQty[]) => {
  return products.reduce((acc, item) => acc + item.quantity, 0);
};

export const getFormattedPrice = (price: number, currency?: string): string => {
  switch (currency) {
    case "EUR":
      return `${price.toFixed(2)} €`;
    case "USD":
      return `${price.toFixed(2)} $`;
    default:
      return `${price.toFixed(2)} €`;
  }
};

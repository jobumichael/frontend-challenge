import Link from "next/link";
import { FC, useState } from "react";
import { Button, IconCart } from "~/components";
import { getFormattedPrice } from "~/util/getFormattedPrice";
import { IProduct } from "./ProductCard.types";

interface IProps {
  product: IProduct;
  handleAddToCart: (product: IProduct) => void;
}

const ProductCard: FC<IProps> = ({ product, handleAddToCart }) => {
  const [loading, setLoading] = useState(false);

  const { name, recommendedRetailPrice, imageUrl, currency, gtin } = product;

  const handleOnClickAddToCart = () => {
    setLoading(true);
    handleAddToCart(product);

    // Fake delay to show loading state
    setTimeout(() => {
      setLoading(false);
    }, 200);
  };

  return (
    <div className="bg-white p-6 flex flex-col rounded-lg shadow-md hover:shadow-lg">
      <Link href={`/product/${gtin}`}>
        <a>
          <img className="p-8 pt-0 rounded-t-lg" src={imageUrl} alt={name} />
        </a>
      </Link>
      <div className="h-20">
        <Link href={`/product/${gtin}`}>
          <a>
            <h5 className="text-md  text-gray-700">{name}</h5>
          </a>
        </Link>
      </div>
      <div className="flex justify-between items-center">
        <span className="text-xl font-bold text-gray-800">
          {getFormattedPrice(recommendedRetailPrice, currency)}
        </span>
        <Button
          styles="w-3/5 inline-flex items-center "
          disabled={loading}
          onClick={handleOnClickAddToCart}
        >
          <IconCart />
          <span className="ml-1">{loading ? "Adding" : "Add to cart"}</span>
        </Button>
      </div>
    </div>
  );
};

export default ProductCard;

export interface IProduct {
  name: string;
  recommendedRetailPrice: number;
  imageUrl: string;
  gtin: string;
  brandName: string;
  categoryName: string;
  currency: string;
}

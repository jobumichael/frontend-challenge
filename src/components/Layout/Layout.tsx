import { FC } from "react";
import { useCartContext } from "~/context/CartContext";
import { getCartQty } from "~/util/getCartQty";
import Footer from "./Footer/Footer";
import Header from "./Header/Header";

interface IProps {
  children: React.ReactNode;
}

const Layout: FC<IProps> = ({ children }) => {
  const { cart } = useCartContext();
  const cartQuantity = getCartQty(Object.values(cart || {}));

  return (
    <>
      <Header cartQuantity={cartQuantity} />
      {children}
      <Footer />
    </>
  );
};

export default Layout;

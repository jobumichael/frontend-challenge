import Link from "next/link";
import { FC } from "react";
import { IconCart, IconLogo } from "~/components";

interface IProps {
  cartQuantity?: number;
}

const Header: FC<IProps> = ({ cartQuantity = 0 }) => {
  return (
    <nav id="header" className="w-full z-30 top-0 py-1 mt-4">
      <div className="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-6 py-3">
        <div className="order-1 md:order-2">
          <Link href="/">
            <a
              className="flex items-center tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl "
              href="#"
            >
              <IconLogo />
              Qogita
            </a>
          </Link>
        </div>

        <div className="order-2 md:order-3 flex items-center" id="nav-content">
          <Link href="/cart">
            <a className="relative pl-3 inline-block no-underline hover:text-black" href="/cart">
              {cartQuantity > 0 && (
                <span className="absolute -top-1 -right-1 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">
                  {cartQuantity}
                </span>
              )}
              <IconCart />
            </a>
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default Header;

import React, { FC } from "react";

const ButtonType = {
  primary: "bg-indigo-500 font-semibold hover:bg-indigo-600 text-sm text-white uppercase rounded",
  icon: "hover:text-white hover:border-1 text-sm  text-center",
};

const ButtonSize = {
  sm: "py-2 px-4 text-xs",
  lg: "py-3 px-6 text-lg",
};

interface IProps {
  size?: "sm" | "lg";
  type?: "primary" | "icon";
  disabled?: boolean;
  styles?: string;
  children?: React.ReactNode;
  onClick?: () => void;
}

const Button: FC<IProps> = ({
  size = "sm",
  type = "primary",
  disabled,
  children,
  styles,
  onClick,
}) => {
  const classNames = `${ButtonType[type]} ${ButtonSize[size]} ${styles} ${
    disabled && "opacity-50 cursor-not-allowed"
  }`;

  return (
    <button onClick={onClick} className={classNames} disabled={disabled}>
      {children}
    </button>
  );
};

export default Button;

import Link from "next/link";
import { FC } from "react";
import { IProduct } from "~/components/types";
import { getFormattedPrice } from "~/util/getFormattedPrice";
import { IconMinus, IconPlus } from "..";
import Button from "../UI/Button/Button";

interface IProps extends IProduct {
  quantity: number;
  onRemove: (gtin: string) => void;
  onUpdateQuantity: (gtin: string, quantity: number) => void;
}

const CartItem: FC<IProps> = (props) => {
  const {
    name,
    recommendedRetailPrice,
    imageUrl,
    gtin,
    quantity,
    brandName,
    currency,
    onRemove,
    onUpdateQuantity,
  } = props;

  const handleOnChangeQtyInput = (e: any) => {
    const re = /^[0-9\b]+$/;

    if (e.target.value === "" || re.test(e.target.value)) {
      onUpdateQuantity(gtin, Number(e.target.value));
    }
  };

  return (
    <div className="flex items-center hover:bg-gray-100 -mx-8 px-6 py-5 justify-between flex-col md:flex-row border-b-2">
      <div className="flex sm:w-full md:w-2/5">
        <div className="w-1/3 h-24">
          <img className="h-24" src={imageUrl} alt={name}></img>
        </div>
        <div className="flex flex-col justify-between flex-grow w-2/3">
          <Link href={`/product/${gtin}`}>
            <a>
              <span className="font-bold text-sm">{name}</span>
            </a>
          </Link>
          <span className=" text-xs text-gray-500 ">{brandName}</span>
          <a
            href="#"
            className="font-semibold text-red-500 hover:text-red-500 text-xs w-fit"
            onClick={() => onRemove(gtin)}
          >
            Remove
          </a>
        </div>
      </div>
      <div className="flex justify-between sm:w-full md:w-1/2 mt-4 md:mt-0">
        <div className="flex justify-center w-1/5 ml-20 md:ml-0">
          <Button
            disabled={quantity === 1}
            onClick={() => onUpdateQuantity(gtin, quantity - 1)}
            type="icon"
            styles="px-0"
          >
            <IconMinus />
          </Button>

          <input
            className="mx-2 border text-center w-8"
            type="text"
            onChange={(e) => handleOnChangeQtyInput(e)}
            value={quantity}
          ></input>

          <Button onClick={() => onUpdateQuantity(gtin, quantity + 1)} type="icon" styles="px-0">
            <IconPlus />
          </Button>
        </div>

        <span className="text-center font-semibold text-sm mr-3">
          {getFormattedPrice(recommendedRetailPrice * quantity, currency)}
        </span>
      </div>
    </div>
  );
};

export default CartItem;

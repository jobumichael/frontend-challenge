import Link from "next/link";
import { FC } from "react";
import { Button } from "~/components";

interface IProps {
  currentPage: number;
  pageSize?: number;
  totalCount: number;
}

const labelClasses = "font-semibold text-gray-900";

const Pagination: FC<IProps> = ({ currentPage, pageSize = 20, totalCount }) => {
  const hasNextPage = currentPage < Math.ceil(totalCount / pageSize);
  const hasPrevPage = currentPage > 1;
  const currentPageStart = (currentPage - 1) * pageSize + 1;
  const currentPageEnd = currentPage * pageSize;
  const nextPage = currentPage + 1;
  const prevPage = currentPage - 1;

  return (
    <div className="flex flex-col items-center">
      <span className="text-sm text-gray-700">
        Showing <span className={labelClasses}>{currentPageStart}</span> to{" "}
        <span className={labelClasses}>{currentPageEnd}</span> of{" "}
        <span className={labelClasses}>{totalCount}</span> products
      </span>
      <div className="inline-flex mt-2 xs:mt-0">
        <Link href={`?page=${prevPage}`}>
          <Button styles="rounded-r-none rounded-l " disabled={!hasPrevPage}>
            Prev
          </Button>
        </Link>

        <span></span>
        <Link href={`?page=${nextPage}`}>
          <Button
            styles="rounded-r rounded-l-none border-0 border-l border-white-700"
            disabled={!hasNextPage}
          >
            Next
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Pagination;

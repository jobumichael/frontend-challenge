import { useState } from "react";

interface IResponse {
  data: any;
  loading: boolean;
  error: any;
  request: (data: any) => void;
}

export const useFetch = (apiFunc: any): IResponse => {
  const [data, setData] = useState(null);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  const request = async (...args: any) => {
    setLoading(true);
    try {
      const result = await apiFunc(...args);
      const data = await result.json();

      setData(data);
    } catch (err: any) {
      setError(err.message || "Unexpected Error!");
    } finally {
      setLoading(false);
    }
  };

  return {
    data,
    error,
    loading,
    request,
  };
};

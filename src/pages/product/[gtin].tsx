import { FC } from "react";
import { Layout } from "~/components";
import { ProductDetails } from "~/modules";

interface IProps {
  gtin: string;
}

const Product: FC<IProps> = ({ gtin }) => {
  return (
    <Layout>
      <ProductDetails gtin={gtin} />
    </Layout>
  );
};

type TServerSideProps = { params: { gtin: string } };

export async function getServerSideProps({ params: { gtin } }: TServerSideProps) {
  return { props: { gtin } };
}

export default Product;

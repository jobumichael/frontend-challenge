import { FC } from "react";
import { Layout } from "~/components";
import { Products } from "~/modules";

interface IProps {
  page: number;
}

const HomePage: FC<IProps> = ({ page }) => {
  return (
    <Layout>
      <Products page={page} />
    </Layout>
  );
};

export async function getServerSideProps({ query }: { query: { page: string } }) {
  const page = query.page;

  return {
    props: {
      page: page ? parseInt(page) : 1,
    },
  };
}

export default HomePage;

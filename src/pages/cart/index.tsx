import Link from "next/link";
import { CartItem, IconLeftArrow, Layout } from "~/components";
import { useCartContext } from "~/context/CartContext";
import { getCartQty } from "~/util/getCartQty";
import { getCartTotal } from "~/util/getCartTotal";

const CartPage = () => {
  const { cart, updateCart, removeCartItem } = useCartContext();

  const handleRemoveItem = (gtin: string) => {
    removeCartItem(gtin);
  };
  const handleUpdateItem = (gtin: string, quantity: number) => {
    if (cart && cart?.[gtin]) {
      updateCart({ ...cart[gtin], quantity });
    }
  };

  const cartQuantity = getCartQty(Object.values(cart || {}));

  const hasItems = cart && Object.keys(cart).length > 0;

  return (
    <Layout>
      <div className="md:container mx-auto mt-10">
        <div className="flex md:shadow-lg">
          <div className="w-full bg-white px-10 py-10">
            <div className="flex justify-between  pb-8">
              <h1 className="font-semibold text-2xl">Shopping Cart</h1>
            </div>

            {!hasItems && (
              <div className="flex justify-center">
                <div className="text-center">
                  <h2 className="text-gray-600 text-2xl">Your cart is empty!</h2>
                  <Link href="/">
                    <a className="text-indigo-600 block mt-10">Shop now</a>
                  </Link>
                </div>
              </div>
            )}

            <div>
              {Object.values(cart || {}).map((item) => (
                <CartItem
                  {...item}
                  onRemove={handleRemoveItem}
                  onUpdateQuantity={handleUpdateItem}
                />
              ))}
            </div>
            {hasItems && (
              <div className="flex mt-10 mb-5 align-middle justify-end">
                <h3 className="text-xl p-0 m-0">Subtotal({`${cartQuantity}`} Items) : </h3>
                <h2 className="font-semibold text-2xl p-0 m-0 ml-4">
                  {getCartTotal(Object.values(cart || {}))}
                </h2>
              </div>
            )}
          </div>
        </div>

        <div className="flex justify-between mt-10 mb-5 ml-10 md:ml-0">
          <Link href="/">
            <a href="#" className="flex font-semibold text-indigo-600 text-sm mt-10">
              <IconLeftArrow />
              Continue Shopping
            </a>
          </Link>
        </div>
      </div>
    </Layout>
  );
};

export default CartPage;

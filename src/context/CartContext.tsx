import { createContext, FC, useContext, useState } from "react";
import { IProduct } from "~/components/types";

interface ICart extends IProduct {
  quantity: number;
}

interface ICartObject {
  [key: string]: ICart;
}

interface ICartContext {
  cart: ICartObject | null;
  updateCart: (data: ICart) => void;
  removeCartItem: (gtin: string) => void;
}

const getStoredCart = () => {
  if (typeof window === "undefined") {
    return {} as ICart;
  } else {
    const storedCart = localStorage.getItem("cart");
    return storedCart ? JSON.parse(storedCart) : ({} as ICart);
  }
};

const storeCart = (cart: ICartObject) => {
  if (typeof window !== "undefined") {
    localStorage.setItem("cart", JSON.stringify(cart));
  }
};

const CartContext = createContext<ICartContext>({
  cart: null,
  updateCart: () => {},
  removeCartItem: () => {},
});

export const CartProvider: FC = ({ children }) => {
  const [cart, setCart] = useState<ICartObject>(getStoredCart());

  const updateCart = (data: ICart) => {
    const newCart = { ...cart, [data.gtin]: data };
    setCart(newCart);
    storeCart(newCart);
  };

  const removeCartItem = (gtin: string) => {
    const newCart = { ...cart };
    delete newCart[gtin];

    setCart(newCart);
    storeCart(newCart);
  };

  return (
    <CartContext.Provider
      value={{
        cart,
        updateCart,
        removeCartItem,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export const useCartContext = () => {
  return useContext(CartContext);
};
